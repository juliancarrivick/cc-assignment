/**
*	linked_list provides functions and structs that are to be used as a linked
*	list. Any linked list or its node are all allocated on the heap.
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Date:		May 2014
*	Unit:		Computer Communications 200
*/




#include <cnet.h>
#include <stdlib.h>
#include <stdio.h>
#include "assignment_datatypes.h"
#include "linked_list.h"




/**
*	createLinkedList
*		Purpose: To create a new, empty linked list for generic use.
*		Inport: None
*		Export: A pointer to the newly create linked list.
*/
LinkedList *createLinkedList()
{
	LinkedList *list;

	list = (LinkedList*)malloc( sizeof( LinkedList ) );
	list->head = NULL;
	list->tail = NULL;
	list->count = 0;

	return list;
}


/**
*	insertFront
*	Purpose: To insert a value into the front of a linked list.
*	Import: A linked list pointer, the list to add to.
*			A void pointer, the data to add to the linked list.
*	Export: None.
*/
void insertFront( LinkedList* list, Frame data )
{
	Node *newNode;

	newNode = (Node*)malloc( sizeof( Node ) );
	newNode->data = data;
	newNode->prev = NULL;

	if ( list->count == 0 )
		list->tail = newNode;
	else
		list->head->prev = newNode;

		
	list->count++;
	newNode->next = list->head;
	list->head = newNode;

}


/**
*	insertBack
*	Purpose: To insert a value into the back of a linked list.
*	Import: A linked list pointer, the list to add to.
*			A void pointer, the data to add to the linked list.
*	Export: None.
*/
void insertBack( LinkedList* list, Frame data )
{
	Node *newNode;

	newNode = (Node*)malloc( sizeof( Node ) );
	newNode->data = data;
	newNode->next = NULL;

	if ( list->count == 0 )
		list->head = newNode;
	else
	{
		list->tail->next = newNode;
		newNode->prev = list->tail;
	}

	list->tail = newNode;
	list->count++;
}


/**
*	peekFront
*	Purpose: To return the value of the first item in the list.
*	Import: A Linked List pointer, the list to get from.
*	Export: A void pointer, the data of the head node.
*/
Frame peekFront( LinkedList* list )
{
	return list->head->data;
}


/**
*	peekBack
*	Purpose: To return the value of the last item in the list.
*	Import: A Linked List pointer, the list to get from.
*	Export: A void pointer, the data of the head node.
*/
Frame peekBack( LinkedList* list )
{
	return list->tail->data;
}



/**
*	removeFront
*	Purpose: Removes the first item in the linked list and returns its value. Also
*		frees the removed nodes memory.
*	Import:	A LinkedList pointer, the list to remove from.
*	Export: A Void pointer, the value of the old front node.
*	Comments: If there are no items in the list, then NULL is returned.
*/
Frame removeFront( LinkedList* list )
{
	Frame data;
	Node* oldNode;

	if ( list->count > 0 )
	{
		data = list->head->data;
		oldNode = list->head;

		if ( list->count == 1 )
		{
			list->head = NULL;
			list->tail = NULL;
		}
		else
		{
			list->head = list->head->next;
			list->head->prev = NULL;
		}

		list->count--;
		free( oldNode );
	}

	return data;	
}


/**
*	removeBack
*	Purpose: To removes the last item in the linked list and returns its value. 
*		Also frees the removed node's memory.
*	Import:	A LinkedList pointer, the list to remove from.
*	Export: A Void pointer, the value of the old back node.
*	Comments: If there are no items in the list, then NULL is returned.
*/
Frame removeBack( LinkedList* list )
{
	Frame data;
	Node* oldNode;

	if ( list->count > 0 )
	{
		data = list->tail->data;
		oldNode = list->tail;

		if ( list->count == 1 )
		{
			list->head = NULL;
			list->tail = NULL;
		}
		else
		{
			list->tail = list->tail->prev;
			list->tail->next = NULL;
		}

		list->count--;
		free( oldNode );
	}

	return data;
}


/**
*	isEmpty
*	Purpose: To determin whether the linked list is empty or not.
*	Import: A LinkedList Pointer
*	Export: boolean, specifies whether the list is empty or not.
*/
bool isEmpty( LinkedList* list )
{
	return list->count == 0;
}