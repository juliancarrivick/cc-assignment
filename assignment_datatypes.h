/**
*	This file contains the declarations of the datatypes used in
*	assignment.c
*	It was neccassery to put them into a seperate file as CNET complains if
*	variables are unused or functions are never defined. Since assignment.h
*	had variables and functions which were static, they were only defined in
*	assignemt.c and such when assignment.h was #included, CNET wasn't happy
*	that they weren't defined in linked_list.c or queue.c
*
*	Hence, the datatypes are declared in this file, and this file is used by
*	all the .c files.
*	
*	Created by: Julian Carrivick
*	Student ID: 16164442
*	Date: 20140404
*	Unit: Computer Communications 200
*/



/****************
*	DATA TYPES 	*
****************/

/**
*	A simple enumeration to distinguish between different types of frames that
*	can be sent. It is either a:
*		DL_DATA: A frame containing data
*		DL_ACK: A frame containing an acknowledgement.
*/
typedef enum
{
	DL_DATA,
	DL_ACK
} FrameType;


/**
*	A struct used to describe a message that has been passed down from the
*	application layer and needs to be transmitted.
*/
typedef struct
{
	char data[MAX_MESSAGE_SIZE]; 	// MAX_MESSAGE_SIZE defined in cnet.h
} Message;							// At time of writing this value is 32768


/**
*	A struct used to represent a single packet that has a origin, destination
*	and length of the message along with the message itself.
*/
typedef struct
{
	CnetAddr origin, dest;
	unsigned int messageLength;
	Message message;
} Packet;


/**
*	A struct used to represent a single frame that has a kind, packet length,
*	checksum and the packet itself.
*/
typedef struct
{
	FrameType type;
	int frameNumber;
	unsigned int packetLength;
	int checksum;
	Packet packet;
} Frame;