/*
*	queue provides functions and structs that are to be used as a Queue.
*	All data is stored on the heap. This file requires the presence of 
*	linked_list.h and linked_list.c
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Date:		May 2014
*	Unit:		Computer Communications 200
*/




#include <cnet.h>
#include "assignment_datatypes.h"
#include "linked_list.h"
#include "queue.h"




/**
*	createQueue
*	Returns a freshly created Queue variable.
*/
Queue createQueue()
{
	return (Queue)createLinkedList();
}


/**
*	enQueue
*	Places a Frame in the queue.
*/
void enQueue( Queue queue, Frame frame )
{
	insertBack( (LinkedList*)queue, frame );
}


/**
*	deQueue
*	Removes a frame from the queue and returns it.
*/
Frame deQueue( Queue queue )
{
	return removeFront( (LinkedList*)queue );
}


/**
*	peek
*	Returns the Frame currently at the front of the queue but does not remove
*	it.
*/
Frame peek( Queue queue )
{
	return peekFront( (LinkedList*)queue );
}


/**
*	queueLength
*	Returns the length of a specified queue
*/
int queueLength( Queue queue )
{
	return ((LinkedList*)queue)->count;
}