/**
*	This file contains the neccassery data types and functions used by
*	assignment.c 
*	
*	Created by: Julian Carrivick
*	Student ID: 16164442
*	Date: 20140404
*	Unit: Computer Communications 200
*/




/********************
*	DEFINITIONS		*
********************/


/* Used to determine the length of an arbitary packet */
#define PACKETLENGTH(p) (sizeof(Packet) - sizeof(Message) + (p).messageLength)

/* Used to determine the length of an arbitary frame */
#define FRAMELENGTH(f) (sizeof(Frame) - sizeof(Packet) + (size_t)(f).packetLength)

/* Defines how many nodes there are in the topology. */
#define NUMNODES 5




/************************
*	GLOBAL VARIABLES	*
************************/

/**
*	routingTable
*	This variable descibes the links that each node has to take to get to 
*	another in the topology.
*	The value in the table corresponds to the link number between the two 
*	nodes.
*	The rows and columns correspond to the nodeinfo.nodenumber for each node:
*		0 - Perth
*		1 - Karratha
*		2 - Kalgoorlie
*		3 - Geraldton
*		4 - Albany
*	To read, you go from ROM to COLUMN.
*		ie. To go from Perth (0) to Geraldton (3) you would read row 0, column
*			3 to give you a linkNumber of 3 which is the one which connects
*			the two nodes.
*		OR  To go from Geraldton (3) to Kalgoorlie (2) you would need to use
*			link 1.
*		A value of 0 indicates the LOOPBACK link, ie back to the same node.
*/
static int routingTable[NUMNODES][NUMNODES] = 
								{		/* To Address */
									{  0,  1,  2,  3,  4 },
									{  1,  0,  2,  1,  1 },
			/* From address */		{  1,  2,  0,  1,  1 },
									{  1,  1,  1,  0,  2 },
									{  1,  1,  1,  2,  0 }
								};


/**
*	buffer
*	This variable holds the buffer for each link connected to the each node.
*	It is implemented as a queue.
*/
static Queue buffer[NUMNODES][NUMNODES];


/**
*	frameExpected
*	This 2D array holds the frame number that each node is expecting from each
*	of its links. To start off, each of them expects frame 0.
*	Where there are -1's are fillers as Perth has four connections but each of
*	the other nodes only have two.
*	This will change between 0 and 1 as frames are recieved.
*/
static int frameExpected[NUMNODES][NUMNODES] = 		
								{		/* Link Number */
									{  0,  0,  0,  0,  0 },
									{  0,  0,  0, -1, -1 },
			/*	Node Number */		{  0,  0,  0, -1, -1 },
									{  0,  0,  0, -1, -1 },
									{  0,  0,  0, -1, -1 }
								};


/**
*	ackExpected
*	This 2D array holds the acknowledgment number that each node is expecting
*	from each of its links. To start off, each of them expects frame 0.
*	Where there are -1's are fillers as Perth has four connections but each of
*	the other nodes only have two.
*	This will change between 0 and 1 as frames are recieved.
*/
static int ackExpected[NUMNODES][NUMNODES] =
								{		/* Link Number */
									{  0,  0,  0,  0,  0 },
									{  0,  0,  0, -1, -1 },
			/*	Node Number */		{  0,  0,  0, -1, -1 },
									{  0,  0,  0, -1, -1 },
									{  0,  0,  0, -1, -1 }
								};



/**
*	timers
*	This variable is an array of timers which are used when a DL_DATA packet
*	is sent. If an DL_ACK is not recieved in the time before the timer expires
*	then the frame is resent.
*	They are all initialised as NULL to begin with.
*	The array index is equal to the nodeinfo.nodenumber value.
*/
static CnetTimerID timers[NUMNODES][NUMNODES] = 
				{ 			/* Link Number */
					{ NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER },
					{ NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER },
/* Node Number */	{ NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER },
					{ NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER },
					{ NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER, NULLTIMER }
				};




/****************
*	FUNCTIONS 	*
****************/

/**
*	receiveFromApplication
*	This function runs on the EV_APPLICATIONREADY event. ie When there is data
*	to be received from the Application Layer.
*/
static void receiveFromApplication(CnetEvent, CnetTimerID, CnetData);


/**
*	receiveFromPhysical
*	This function runs on the EV_PHYSICALREADY even. ie When there is data
*	ready to be received from the Physical Layer.
*	This function is essentially the recieving end of the DataLink Layer, 
*	indeed it could even be called upToDataLink if it increased clarity.
*/
static void receiveFromPhysical(CnetEvent, CnetTimerID, CnetData);


/**
*	acknowledgementTimeout
*	This function runs on the EV_TIMER1 event. ie When the timer runs out.
*	This is engineered to occur after an appropiate amount of time has passed
*	before a timeout is to be expected.
*	The CnetData parameter contains the transmission link the the frame is to 
*	be transmitted on.
*/
static void acknowledgementTimeout(CnetEvent, CnetTimerID, CnetData);


/**
*	showState
*	Shows the state of the current node including data such as:
*
*/
static void showState(CnetEvent, CnetTimerID, CnetData);


/**
*	downToNetwork
*	Takes a message, its length and the destination to send to, calculates
*	which link to send on, constructs the packet then passes it down to the
*	DataLink Layer for transmission.
*/
static void downToNetwork(Message, unsigned int, CnetAddr);


/**
*	downToDataLink
*	Takes a Packet, the Type of Frame to send, its length and which link to 
*	send on, packages it all into a frame and transmits on the physical media.
*/
static void downToDataLink(Packet, FrameType, unsigned int, int);


/**
*	upToNetwork
*	Takes a packet for processing. If the packet is destined for this node
*	then it will pass it up to the application layer, otherwise it will send
*	it along to where it needs to go.
*/
static void upToNetwork(Packet);


/**
*	transmitAck
*	Transmits an acknowledgment over the specified link with the frame number
*	it is acknowledging.
*/
static void transmitAck(int transmissionLink, int expectedFrame);


/**
*	shutdown
*	Runs some cleanup tasks when the CNET is closed
*/
static void shutdown();


/**
*	transmitNextFrame
*	Transmits the next frame in the queue for a given link (nodenumber can be
*		found from nodeinfo.nodenumber)
*/
static void transmitNextFrame(int transmissionLink);


/**
*	calcTimeOut
*	Calculates the value of a timeout given a transmissionLink and frame 
*		Length.
*/
static CnetTime calcTimeOut(int transmissionLink, size_t frameLength);


/**
*	getIntermediateNode
*	Given a destination, this function works out if there is a intermediate 
*		node between the origin and destination. If there is, it returns
*		the nodenumber of this intermediate node. If there isn't, it simply
*		returns the nodenumber of the destination.
*/
static CnetAddr getIntermediateNode(CnetAddr destination);