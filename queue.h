/*
*	queue provides functions and structs that are to be used as a Queue.
*	All data is stored on the heap. This file requires the presence of 
*	linked_list.h and linked_list.c
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Date:		May 2014
*	Unit:		Computer Communications 200
*/




/****************
*	DATA TYPES	*
****************/

/**
*	Queue
*	A type which holds a Linked List Pointer. It is done this way so that
*	a user does not need to worry about pointers and such and can just pass
*	the functions a Queue variable.
*/
typedef LinkedList* Queue;




/****************
*	FUNCTIONS 	*
****************/


/**
*	createQueue
*	Returns a freshly created Queue variable.
*/
Queue createQueue();


/**
*	enQueue
*	Places a Frame in the queue.
*/
void enQueue( Queue, Frame );


/**
*	deQueue
*	Removes a frame from the queue and returns it.
*/
Frame deQueue( Queue );


/**
*	peek
*	Returns the Frame currently at the front of the queue but does not remove
*	it.
*/
Frame peek( Queue );


/**
*	queueLength
*	Returns the length of a specified queue
*/
int queueLength( Queue );