/**
*	This file contains the logic for operation of CNET using the stop and
*	wait protocol.
*	
*	Created by: Julian Carrivick
*	Student ID: 16164442
*	Date: May 2014
*	Unit: Computer Communications 200
*/




#include <cnet.h>
#include <stdlib.h>
#include "assignment_datatypes.h"
#include "linked_list.h"
#include "queue.h"
#include "assignment.h"




/**
*	reboot_node
*	The essential function for CNET, will be called upon the execution of the
*	program for each node.
*/
void reboot_node(CnetEvent ev, CnetTimerID  timer, CnetData data)
{
	int i;

	/* Tell CNET which events we are interested in */
	CHECK(CNET_set_handler( EV_APPLICATIONREADY, receiveFromApplication, 0));
    CHECK(CNET_set_handler( EV_PHYSICALREADY, receiveFromPhysical, 0));
    CHECK(CNET_set_handler( EV_TIMER1, acknowledgementTimeout, 0));
    CHECK(CNET_set_handler( EV_DEBUG0, showState, 0));
    CHECK(CNET_set_handler( EV_SHUTDOWN, shutdown, 0));

    CHECK(CNET_set_debug_string( EV_DEBUG0, "Node State"));

    CNET_enable_application(ALLNODES);

    /* For this node, initialise the queue for each link it has. It is less
    	than or equal to number of links since link 0 is the special loopback 
    	link */
    for (i = 0; i <= nodeinfo.nlinks; i++)
    {
    	buffer[nodeinfo.nodenumber][i] = createQueue();
    }
}




/*******************
* DOWNWARDS LAYERS *
*******************/

/**
*	receiveFromApplication
*	This function runs on the EV_APPLICATIONREADY event. ie When there is data
*	to be received from the Application Layer.
*/
static void receiveFromApplication(CnetEvent ev, CnetTimerID timer,
									CnetData data)
{
	CnetAddr dest;
	Message message;
	size_t length = sizeof(Message);

	CHECK( CNET_read_application( &dest, message.data, &length ) );

	printf("\nDown from Application:\n");
	printf("\tDestination: %d, Message: %s, Length: %d\n", dest,
		message.data, length);

	// Stop more messages from being sent to destination.
	CNET_disable_application(dest);

	downToNetwork(message, (unsigned int)length, dest);
}


/**
*	downToNetwork
*	Takes a message, its length and the destination to send to, calculates
*	which link to send on, constructs the packet then passes it down to the
*	DataLink Layer for transmission.
*/
static void downToNetwork(Message message, unsigned int length, CnetAddr dest)
{
	Packet packet;
	int linkToUse;
	unsigned int packetLength;

	// The intermediate node between origin and destination
	CnetAddr intNode;

	/* Ecapsulating the message into a single packet */
	packet.origin = nodeinfo.nodenumber;
	packet.dest = dest;
	packet.messageLength = length;
	packet.message = message;

	packetLength = PACKETLENGTH(packet);

	linkToUse = routingTable[nodeinfo.nodenumber][dest];

	intNode = getIntermediateNode(dest);

	/* If the destination is not the same as the node the packet is getting
		sent to (ie the intermediate node), disable the application layer for
		that too. */
	if (dest != (int)intNode)
	{
		CNET_disable_application(intNode);
	}

	printf("\tDown To Network, Sending Packet from Node %d to Node %d\n", 
		packet.origin, packet.dest);

	downToDataLink(packet, DL_DATA, packetLength, linkToUse);
}


/**
*	downToDataLink
*	Takes a Packet, its length and which link to send on, packages it all into
*	a frame and transmits on the physical media.
*/
static void downToDataLink(Packet packet, FrameType type, unsigned int length, 
	int transmissionLink)
{
	Frame frame;
	size_t frameLength;
	CnetTime timeout;

	/* New variables for easy access */
	int thisExpectedAck = ackExpected[nodeinfo.nodenumber][transmissionLink];
	Queue q = buffer[nodeinfo.nodenumber][transmissionLink];

	/* Calculate ahead of time what the frame number will be. 
		ie if the length of the queue is an odd number then it should be the 
		other sequence number */
	if ( queueLength(q) % 2 == 1 )
	{ 
		thisExpectedAck = 1 - thisExpectedAck;
	}

	/* Ecapsulate Packet into Frame */
	frame.type = type;
	frame.frameNumber = thisExpectedAck;
	frame.packetLength = length;
	frame.checksum = 0;
	frame.packet = packet;

	frameLength = FRAMELENGTH(frame);
	frame.checksum = CNET_ccitt((unsigned char*)&frame, frameLength);

	/* Add this frame to the outgoing queue */
	enQueue( buffer[nodeinfo.nodenumber][transmissionLink], frame );

	/* Transmit now if it is the only frame in the queue, otherwise wait */
	if ( queueLength(q) == 1 )
	{
		timeout = calcTimeOut(transmissionLink, frameLength);

	/*	The transmission link is passed to the timer so that when the timeout
		function is called it will know which link to send on */
		timers[nodeinfo.nodenumber][transmissionLink] =
		CNET_start_timer(EV_TIMER1, 3 * timeout, (CnetData)transmissionLink);

		printf("\tTimerID: %d for Node %d on Link %d of %dus\n",
			timers[nodeinfo.nodenumber][transmissionLink], nodeinfo.nodenumber, 
			transmissionLink, 3*timeout );
		printf("\tTransmitting data on link %d. Checksum: %d, length: %d, "
		"No: %d\n", transmissionLink, frame.checksum, frameLength, 
			frame.frameNumber, timeout);

		/* Write the frame to the physical connection. */
		CHECK(CNET_write_physical(transmissionLink, &frame, &frameLength));
	}
	else
	{
		printf("\tAdded to queue...\n");
	}
}




/*****************
* UPWARDS LAYERS *
*****************/

/**
*	receiveFromPhysical
*	This function runs on the EV_PHYSICALREADY even. ie When there is data
*	ready to be received from the Physical Layer.
*/
static void receiveFromPhysical(CnetEvent ev, CnetTimerID timer,
									CnetData data)
{
	int link;
	size_t length = sizeof(Frame); // Max amount of data that can be read
	Frame frame, lastSentFrame;
	uint16_t frameChecksum, calculatedChecksum;
	int *thisExpectedFrame, *thisExpectedAck; // Reduces clutter below.

	CHECK(CNET_read_physical(&link, (Frame*)&frame, &length));

	thisExpectedFrame = &frameExpected[nodeinfo.nodenumber][link];
	thisExpectedAck = &ackExpected[nodeinfo.nodenumber][link];

	frameChecksum = frame.checksum;
	frame.checksum = 0; //As this was the value when the checksum was calculated
	calculatedChecksum = CNET_ccitt( (unsigned char*)&frame, length);
	
	/* Check Frame for errors */
	if ( frameChecksum == calculatedChecksum )
	{
		if ( frame.type == DL_DATA )
		{
			printf("\nData Received, Checksum: %d, Frame No: %d\n", 
				calculatedChecksum, frame.frameNumber);

			if (frame.frameNumber == *thisExpectedFrame )
			{
				printf("\tPacket up to Network\n");
				upToNetwork( frame.packet );

				transmitAck(link, *thisExpectedFrame);
				*thisExpectedFrame = 1 - *thisExpectedFrame;
			}
			else
			{
				printf("\tPrevious Frame received, resending ACK on link %d\n", 
					link);
				transmitAck( link, frame.frameNumber );
			}
		}
		else if ( frame.type == DL_ACK )
		{
			printf("\nACK Received, Frame No: %d\n", *thisExpectedAck);

			if ( frame.frameNumber == *thisExpectedAck )
			{
				/* Cleanup after successful transmission */
				CNET_stop_timer( timers[nodeinfo.nodenumber][link] );
				lastSentFrame = deQueue( buffer[nodeinfo.nodenumber][link] );
				*thisExpectedAck = 1 - *thisExpectedAck;

				printf("\tCorrect ACK from Node %d, ", frame.packet.origin);

				if ( queueLength(buffer[nodeinfo.nodenumber][link]) > 0 )
				{
					printf("\tSending next frame in buffer:\n");
					transmitNextFrame( link );
				}
				else // If buffer is empty, re-enable the application layer.
				{
					printf("Queue is empty, re-enabling application.\n");

					if (frame.packet.origin == lastSentFrame.packet.dest)
					{
						CNET_enable_application(frame.packet.origin);
					}
					else
					{
						CNET_enable_application(frame.packet.origin);
						CNET_enable_application(lastSentFrame.packet.dest);
					}
				}

			}
			else
			{
				printf("Incorrect ACK received from Node %d. "
					"Expected: %d, got %d\n", 
					frame.packet.origin, *thisExpectedAck, frame.frameNumber);
			}
		}
	}
	else
	{
		printf("\nBad Frame Received on link %d\n", link);
	}
}


/**
*	upToNetwork
*	Takes a packet for processing. If the packet is destined for this node
*	then it will pass it up to the application layer, otherwise it will send
*	it along to where it needs to go.
*/
static void upToNetwork(Packet packet)
{
	int packetLength;
	int linkToUse;

	/* Had to read these seperately, not straight into CNET_write_application
	*	as it would fail for some reason, but doing this works */
	Message msg = packet.message;
	size_t msgLength = packet.messageLength;

	if ( packet.dest == nodeinfo.nodenumber )
	{
		printf("\tPacket meant for me - up To Application Layer\n");
		CHECK( CNET_write_application( &msg, &msgLength ) );


	}
	else
	{
		packetLength = PACKETLENGTH(packet);
		linkToUse = routingTable[nodeinfo.nodenumber][packet.dest];

		printf("\tPacket not meant for me, sending to node %d over link %d\n", 
			packet.dest, linkToUse);

		CNET_disable_application(packet.dest);

		downToDataLink(packet, DL_DATA, packetLength, linkToUse);
	}
}




/********************
* UTILITY FUNCTIONS *
********************/

/**
*	transmitAck
*	Transmits an acknowledgment over the specified link with the frame number
*	it is acknowledging.
*/
static void transmitAck(int transmissionLink, int expectedFrame)
{
	Message msg;
	Packet packet;
	Frame frame;
	size_t frameLength;

	packet.origin = nodeinfo.nodenumber;
	packet.message = msg;
	packet.messageLength = 0; 

	frame.type = DL_ACK;
	frame.frameNumber = expectedFrame;
	frame.packetLength = PACKETLENGTH(packet);
	frame.checksum = 0;
	frame.packet = packet;

	frameLength = FRAMELENGTH(frame);
	frame.checksum = CNET_ccitt((unsigned char*)&frame, frameLength);

	printf("\nSending Ack on link %d. Checksum: %d, length: %d, No: %d\n", 
		transmissionLink, frame.checksum, frameLength, expectedFrame);

	CHECK(CNET_write_physical(transmissionLink, &frame, &frameLength));
}


/**
*	transmitNextFrame
*	Transmits the next frame in the queue for a given link (nodenumber can be
*		found from nodeinfo.nodenumber)
*/
static void transmitNextFrame(int transmissionLink)
{
	Queue buff;
	Frame nextFrame;
	size_t frameLength;
	CnetTime timeout;

	buff = buffer[nodeinfo.nodenumber][transmissionLink];
	nextFrame = peek(buff);
	frameLength = FRAMELENGTH(nextFrame);

	timeout = calcTimeOut(transmissionLink, frameLength);
	timers[nodeinfo.nodenumber][transmissionLink] = 
		CNET_start_timer(EV_TIMER1, 3 * timeout, (CnetData)transmissionLink);

	printf("\tTimerID: %d for Node %d on Link %d of %dus\n",
		timers[nodeinfo.nodenumber][transmissionLink], nodeinfo.nodenumber, 
		transmissionLink, 3*timeout );
	printf("\tTransmitting data on link %d. Checksum: %d, length: %d, "
		"No: %d\n", 
		transmissionLink, nextFrame.checksum, frameLength, 
		nextFrame.frameNumber, timeout);

	CNET_disable_application(nextFrame.packet.dest);
	CHECK(CNET_write_physical(transmissionLink, &nextFrame, &frameLength));
}


/**
*	acknowledgementTimeout
*	This function runs on the EV_TIMER1 event. ie When the timer runs out.
*	This is engineered to occur after an appropiate amount of time has passed
*	before a timeout is to be expected.
*	The CnetData parameter contains the transmission link the the frame is to 
*	be transmitted on.
*/
static void acknowledgementTimeout(CnetEvent ev, CnetTimerID timer,
									CnetData data)
{
	Frame frame;
	int linkToUse;
	size_t frameLength;
	CnetTime timeout;

	linkToUse = (int)data;
	frame = peek( buffer[nodeinfo.nodenumber][linkToUse] );
	frameLength = FRAMELENGTH(frame);

	/*	The transmission link is passed to the timer so that when the timeout
		function is called it will know which link to send on */
	timeout = calcTimeOut(linkToUse, frameLength);
	timers[nodeinfo.nodenumber][linkToUse] =
		CNET_start_timer(EV_TIMER1, 3 * timeout, (CnetData)linkToUse);

	printf("\nTimeout (ID: %d) to node %d, resending frame.\n", timer, 
		frame.packet.dest);
	printf("\tTimerID: %d for Node %d on Link %d of %dus\n",
		timers[nodeinfo.nodenumber][linkToUse], nodeinfo.nodenumber, 
		linkToUse, 3*timeout );
	printf("\tTransmitting data on link %d. Checksum: %d, length: %d, "
		"No: %d\n", 
		linkToUse, frame.checksum, frameLength, frame.frameNumber, timeout);

	CHECK(CNET_write_physical(linkToUse, &frame, &frameLength));
}


/**
*	getIntermediateNode
*	Given a destination, this function works out if there is a intermediate 
*		node between the origin and destination. If there is, it returns
*		the nodenumber of this intermediate node. If there isn't, it simply
*		returns the nodenumber of the destination.
*/
static CnetAddr getIntermediateNode(CnetAddr destination)
{
	CnetAddr intNode;

	if ( destination == nodeinfo.nodenumber)
	{
		intNode = destination;
	}
	else
	{
		/* If a node is not directly connected to it's destination
		*	Then the intermeddiate node must be perth, given the 
		*	topology 
		*	If the topology changes, or more complex computation needs to 
		*	done, then it can be acheived here */
		intNode = (CnetAddr)0;
	}

	return intNode;
}


/**
*	showState
*	Shows the state of the current node including data such as:
*
*/
static void showState(CnetEvent ev, CnetTimerID timer, CnetData data)
{
	int i;
	Queue q;

	for ( i = 0; i <= nodeinfo.nlinks; i++ )
	{
		q = buffer[nodeinfo.nodenumber][i];
		printf("Items in Queue to node %d: %d\n", i,((LinkedList*)q)->count);
	}
}


/**
*	calcTimeOut
*	Calculates the value of a timeout given a transmissionLink and frame 
*		Length.
*/
static CnetTime calcTimeOut(int transmissionLink, size_t frameLength)
{
	return (frameLength * ((CnetTime)8000000 / 
			linkinfo[transmissionLink].bandwidth) + 
			linkinfo[transmissionLink].propagationdelay);
}


/**
*	shutdown
*	Runs some cleanup tasks when the CNET is closed
*/
static void shutdown()
{
	int i;

	/* Free the queues from the heap */
	for ( i = 0; i <= nodeinfo.nlinks; i++ )
	{
		free( buffer[nodeinfo.nodenumber][i] );
	}
}
