/**
*	linked_list provides functions and structs that are to be used as a linked
*	list. Any linked list or its node are all allocated on the heap.
*	Author: 	Julian Carrivick
*	Student ID:	16164442
*	Date:		May 2014
*	Unit:		Computer Communications 200
*/




/****************
*	DATA TYPES	*
****************/

/**
*	A struct which is the node on a linked list. It is doubly linked to allow 
*	backwards traversal.
*		data - a Frame Struct (defined in assignment.h)
*		next - a pointer to the next node in the Linked List
*		prev - a pointer to the previous node in the Linked List
*/
typedef struct Node
{
	Frame data;
	struct Node *next, *prev;
} Node;


/**
*	The Linked List struct. Also contains a count field for quicj checking
*	of the size of the LinkedList. It is double ended, allowing quick access
*	to the last element.
*		count - the number of elements in the list.
*		head - a pointer to the head of the list.
*		tail - a pointer to the tail of the list.
*/
typedef struct
{
	int count;
	Node *head, *tail;
} LinkedList;




/****************
*	FUNCTIONS 	*
****************/

/**
*	createLinkedList
*		Purpose: To create a new, empty linked list for use.
*		Inport: None
*		Export: A pointer to the newly create linked list.
*/
LinkedList* createLinkedList();


/**
*	insertFront
*	Purpose: To insert a value into the front of a linked list.
*	Import: A linked list pointer, the list to add to.
*			A void pointer, the data to add to the linked list.
*	Export: None.
*/
void insertFront( LinkedList*, Frame );


/**
*	insertBack
*	Purpose: To insert a value into the back of a linked list.
*	Import: A linked list pointer, the list to add to.
*			A void pointer, the data to add to the linked list.
*	Export: None.
*/
void insertBack( LinkedList*, Frame );


/**
*	peekFront
*	Purpose: To return the value of the first item in the list.
*	Import: A Linked List pointer, the list to get from.
*	Export: A void pointer, the data of the head node.
*/
Frame peekFront( LinkedList* );


/**
*	peekBack
*	Purpose: To return the value of the last item in the list.
*	Import: A Linked List pointer, the list to get from.
*	Export: A void pointer, the data of the head node.
*/
Frame peekBack( LinkedList* );


/**
*	removeFront
*	Purpose: Removes the first item in the linked list and returns its value. Also
*		frees the removed nodes memory.
*	Import:	A LinkedList pointer, the list to remove from.
*	Export: A Void pointer, the value of the old front node.
*	Comments: If there are no items in the list, then NULL is returned.
*/
Frame removeFront( LinkedList* );


/**
*	removeBack
*	Purpose: To removes tha last item in the linked list and returns its value. 
*		Also frees the removed node's memory.
*	Import:	A LinkedList pointer, the list to remove from.
*	Export: A Void pointer, the value of the old back node.
*	Comments: If there are no items in the list, then NULL is returned.
*/
Frame removeBack( LinkedList* );


/**
*	isEmpty
*	Purpose: To determin whether the linked list is empty or not.
*	Import: A LinkedList Pointer
*	Export: boolean, specifies whether the list is empty or not.
*/
bool isEmpty( LinkedList* );